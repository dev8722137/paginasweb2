/* Declarar variables*/

const btnCalcular = document.getElementById("btnCalcular");

btnCalcular.addEventListener("click", function () {
  // Obtener los los dtaos de los input
  let valorAuto = document.getElementById("valorAuto").value;
  let pInicial = document.getElementById("porcentaje").value;
  let plazos = document.getElementById("plazos").value;

  //Hacer los calculos
  let pagoInicial = valorAuto * (pInicial / 100);
  let totalFin = valorAuto - pagoInicial;
  let pagoMensual = totalFin / plazos;

  // Mostrar los datos
  document.getElementById("pagoInicial").value = pagoInicial.toFixed(2);
  document.getElementById("totalfin").value = totalFin.toFixed(2);
  document.getElementById("pagoMensual").value = pagoMensual.toFixed(2);

  let vAuto = document.createElement("p");
  vAuto.innerHTML = "<strong>Valor Auto: $  </strong>" + valorAuto;
  document.getElementById("registro").append(vAuto);

  let vInicial = document.createElement("p");
  vInicial.innerHTML = "<strong>Porcentaje:   </strong>" + pInicial + "%";
  document.getElementById("registro").append(vInicial);

  let vPlazos = document.createElement("p");
  vPlazos.innerHTML = "<strong>Plazos:   </strong>" + plazos;
  document.getElementById("registro").append(vPlazos);

  let vPagoInicial = document.createElement("p");
  vPagoInicial.innerHTML =
    "<strong>Pago inicial: $  </strong>" + pagoInicial.toFixed(2);
  document.getElementById("registro").append(vPagoInicial);

  let vTotalfin = document.createElement("p");
  vTotalfin.innerHTML =
    "<strong>Total final: $  </strong>" + totalFin.toFixed(2);
  document.getElementById("registro").append(vTotalfin);

  let vPagoMes = document.createElement("p");
  vPagoMes.innerHTML =
    "<strong>Pago mensual: $  </strong>" + pagoMensual.toFixed(2);
  document.getElementById("registro").append(vPagoMes);

  let hr = document.createElement("hr");
  document.getElementById("registro").append(hr);
});

function arribaMouse() {
  let parrafo = document.getElementById("pa");
  parrafo.style.color = "#FF00FF";
  parrafo.style.fontSize = "25px";
  parrafo.style.textAlign = "justify";
}

function salirMouse() {
  let parrafo = document.getElementById("pa");
  parrafo.style.color = "red";
  parrafo.style.fontSize = "17px";
  parrafo.style.textAlign = "center";
}

function limpiar() {
  let parrafo = document.getElementById("pa");

  parrafo.innerHTML = "";
  // parrafo.textContent="";
}

/* MANAJEO DE ARREGLOS*/
//DECLARACIÓN DE ARRAY CON ELEMENTOS ENTEROS

let arreglo = [4, 89, 30, 10, 34, 89, 10, 5, 8, 28];
function mostrarArray(arreglo) {
  for (let con = 0; con < arreglo.length; con++) {
    console.log(con + ":" + arreglo[con]);
  }
}

// FUNCIÓN PARA LLENAR CON VALORES ALEATORIOS AL ARRAY
// function aleatorios(arreglo) {
//   let tamano = document.getElementById("cantidad").value;
//   for (let x = 0; x < tamano; x++) {
//     arreglo[x].push(Math.random()*10).toFixed(2);
//   }


// }