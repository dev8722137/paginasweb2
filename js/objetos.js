let alumnos = [
  {
    matricula: "2021030118",
    nombre: "Ibarra Hernández Ángel Antonio",
    grupo: "TI-73",
    carrera: "Tecnologias de la informacion",
    foto: "/img/angel.jpeg",
  },
  {
    matricula: "2020030664",
    nombre: "Burgueño Barraza Octavio de Jesús",
    grupo: "TI-73",
    carrera: "Tecnologias de la informacion",
    foto: "/img/Octavio.jpeg",
  },
  {
    matricula: "2021030148",
    nombre: "Vázquez Gamboa Luis Enrique",
    grupo: "TI-73",
    carrera: "Tecnologias de la informacion",
    foto: "/img/Kike.jpeg",
  },
  {
    matricula: "2021030266",
    nombre: "Gonzaléz Ramirez José Manuel",
    grupo: "TI-73",
    carrera: "Tecnologias de la informacion",
    foto: "/img/Manu17.jpeg",
  },
  {
    matricula: "2021030008",
    nombre: "Landeros Andrade Maria Estrella",
    grupo: "TI-73",
    carrera: "Tecnologias de la informacion",
    foto: "/img/Maria.jpeg",
  },
  {
    matricula: "2019030391",
    nombre: "Salzar Bustamante Alan Arturo",
    grupo: "TI-73",
    carrera: "Tecnologias de la informacion",
    foto: "/img/Alan.jpeg",
  },
  {
    matricula: "2020030714",
    nombre: "Ruiz Guerrero Axel Jovani",
    grupo: "TI-73",
    carrera: "Tecnologias de la informacion",
    foto: "/img/Jovani.jpeg",
  },
  {
    matricula: "2021030101",
    nombre: "Plazola Arangure Yohan Alek",
    grupo: "TI-73",
    carrera: "Tecnologias de la informacion",
    foto: "/img/Yohan.jpeg",
  },
  {
    matricula: "2020030321",
    nombre: "Ontiveros Govea Yair Alejandro",
    grupo: "TI-73",
    carrera: "Tecnologias de la informacion",
    foto: "/img/govea.jpeg",
  },
  {
    matricula: "2021030262",
    nombre: "Qui Mora Ángel Ernesto",
    grupo: "TI-73",
    carrera: "Tecnologias de la informacion",
    foto: "/img/Qui.jpeg",
  },
  {
    matricula: "2021030262",
    nombre: "Melissa Osuna Cardenas",
    grupo: "TI-73",
    carrera: "Tecnologias de la informacion",
    foto: "/img/Melissa.jpeg",
  }
];

console.log("Matricula:" + alumnos.matricula);
console.log("Nombre:" + alumnos.nombre);

alumnos.nombre = "Acosta Diaz María";
console.log("Nuevo Nombre:" + alumnos.nombre);

// Objetos compuestos
let cuentaBanco = {
  numero: "1001",
  banco: "Banorte",
  cliente: {
    nombre: "jose Lopez",
    fechaNac: "2020-01-01",
    sexo: "M",
  },
  saldo: "10000",
};

console.log("Nombre: " + cuentaBanco.cliente.nombre);
console.log("Saldo: " + cuentaBanco.saldo);

cuentaBanco.cliente.sexo = "F";
console.log("Sexo: " + cuentaBanco.cliente.sexo);

// Arreglo de prodectos
let productos = [
  {
    codigo: "1001",
    descripcion: "atun",
    precio: "34",
  },
  {
    codigo: "1002",
    descripcion: "Jabon de polvo",
    precio: "23",
  },
  {
    codigo: "1003",
    descripcion: "Harina",
    precio: "43",
  },
  {
    codigo: "1004",
    descripcion: "pasta dental",
    precio: "78",
  },
];

// Mostrar un atributo de un elemento de un abjeto del array list
console.log("La descripcion: " + productos[0].descripcion);

// Mostrar todos los elementos del arreglo

for (let x = 0; x < productos.length; x++) {
  console.log("======================================================");
  console.log("PRODUCTO #" + (x + 1));
  console.log("Codigo: " + productos[x].codigo);
  console.log("La descripcion: " + productos[x].descripcion);
  console.log("Precio: $" + productos[x].precio);
  console.log("======================================================");
}

// Impresión de datos

// Selecciona el elemento tbody donde se mostrarán los alumnos
const tbodyElement = document.getElementById("alumnos-tbody");

// Recorre el arreglo de alumnos y crea una fila de tabla para cada uno
for (let i = 0; i < alumnos.length; i++) {
    const alumnoActual = alumnos[i];
    const trElement = document.createElement("tr");

    // Crea una estructura HTML con todos los campos del alumno
    trElement.innerHTML = `
        <td><img src="${alumnoActual.foto}" alt="${alumnoActual.nombre}" width="100"></td>
        <td>${alumnoActual.nombre}</td>
        <td>${alumnoActual.matricula}</td>
        <td>${alumnoActual.grupo}</td>
        <td>${alumnoActual.carrera}</td>
    `;

    // Agrega la fila a la tabla
    tbodyElement.appendChild(trElement);
}

